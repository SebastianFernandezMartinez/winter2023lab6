public class Jackpot{
    public static void main(String[]args){
        System.out.println("Welcome to the game of dice!");
        Board board = new Board();
        Die die = new Die();
        boolean gameOver = false;
        int numOfTilesClosed = 0;
        
        while(!gameOver){
            System.out.println(board);
            
            if(board.playATurn()){
                gameOver = true;
            }else{
                numOfTilesClosed = 1;
            }
        }
        
        if(numOfTilesClosed >= 7){
            System.out.println("The player reached the jackpot and won!");
        }else {
            System.out.println("The playet didn't reach the jackpot LOSS");
        }
    }
        
}