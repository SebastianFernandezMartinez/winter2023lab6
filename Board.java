public class Board{
	private Die dice1;
	private Die dice2;
	private boolean[] tile;
	
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.tile = new boolean[12];
	}
	
    
    
	public String toString(){
		String tileValue = "";
		
		for(int i = 0; i < tile.length; i++){
			if(!tile[i])
				tileValue += (i + 1);
			else
				tileValue += "X";
				
			tileValue += " ";
		}
		
		return tileValue;
	}
	
	public boolean playATurn(){
		int dice1Roll = dice1.roll();
		int dice2Roll = dice2.roll();
		System.out.println(dice1);
		System.out.println(dice2);

		int diceSum = dice1Roll + dice2Roll;
		
		if(!tile[diceSum - 1]){
			tile[diceSum - 1] = true;
			System.out.println("Closing tile equal to sum: " + diceSum);
			return false;
		}else if(!tile[dice1Roll - 1]){
			tile[dice1Roll-1] = true;
			System.out.println("Closing tile with the same value as die one: " + dice1Roll);
			return false;
		}else if(!tile[dice2Roll - 1]){
			tile[dice2Roll-1] = true;
			System.out.println("Closing tile with the same value as die one: " + dice2Roll);
			return false;
		}else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}